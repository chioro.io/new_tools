const Toolpackage = require('chioro-toolbox/toolpackage')
const base = require('chioro-toolbox/toolbase')

// Toolbox extensions are organized as "Tool Packages".
// A ToolPackage is a collection of tool descriptions, including tests.
// Exporting a ToolPackage object makes a JS module a toolbox extension (see last line)
// base refers to the standard chioro library and can be used anywhere in the code with
// the following syntax: base.nameOfTheStandardFunction

const tools = new Toolpackage("My great toolbox extension")


function niceFunction_2(input1, input2) {
    return base.upperCaseText(input1) + " " + base.lowerCaseText(input2);
}
tools.add({
    id: "niceFunction_2",
    impl: niceFunction_2,
    aliases: {
        en: "niceFunction_2",
        de: "netteFunktion_2"
    },
    args: {
        en: "input1, input2",
        de: "eingabe1, eingabe2"
    },
    tags: ["tag1", "tag2", "esm"],
    tests: () => {
        tools.expect(niceFunction_2("hello", "world")).toBe('HELLO world');
        tools.expect(niceFunction_2("Helping", "World")).toBe('HELPING world');
    }
})

function happyFunction(name) {
    return base.upperCaseText(name) + " is happy";
}
tools.add({
    id: "happyFunction",
    impl: happyFunction,
    aliases: {
        en: "happyFunction",
        de: "happyFunktion"
    },
    args: {
        en: "input",
        de: "eingabe"
    },
    tags: ["tag1", "tag2", "esm"],
    tests: () => {
        tools.expect(happyFunction("ESM")).toBe('ESM is happy');
        tools.expect(happyFunction("Tomato")).toBe('TOMATO is happy');
    }
})

function sadFunction(name) {
    return base.upperCaseText(name) + " is sad";
}
tools.add({
    id: "sadFunction",
    impl: sadFunction,
    aliases: {
        en: "sadFunction",
        de: "sadFunktion"
    },
    args: {
        en: "input",
        de: "eingabe"
    },
    tags: ["tag1", "tag2", "esm"],
    tests: () => {
    }
})

function verySadFunction(name) {
    return base.upperCaseText(name) + " is very sad";
}
tools.add({
    id: "verySadFunction",
    impl: verySadFunction,
    aliases: {
        en: "verySadFunction",
        de: "sehrSadFunktion"
    },
    args: {
        en: "input",
        de: "eingabe"
    },
    tags: ["tag1", "tag2", "esm"],
    tests: () => {
    }
})

function not(input) {
    return !input;
}
tools.add({
    id: "not",
    impl: not,
    aliases: {
        en: "not",
        de: "nicht"
    },
    args: {
        en: "input",
        de: "eingabe"
    },
    tags: ["tag1", "tag2", "esm"],
    tests: () => {
    }
})
//-------------WRITE YOUR FUNCTIONS ABOVE THIS LINE------------------
tools.exportAll(exports)
